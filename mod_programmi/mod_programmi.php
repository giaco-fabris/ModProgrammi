<?php
defined('_JEXEC') or die('Restricted access');
//error_reporting (E_ALL);
require_once dirname(__FILE__).'/helper.php';
//$user = ModProgrammiHelper::getUser();
$class_sfx	= htmlspecialchars($params->get('class_sfx'));

if (isset($_POST['submit'])){
	$input = JFactory::getApplication()->input;
	$post = $input->getArray($_POST);
	$file = $input->files->get('doc');
	ModProgrammiHelper::processForm($post, $file, $params);
}
require JModuleHelper::getLayoutPath('mod_programmi');
