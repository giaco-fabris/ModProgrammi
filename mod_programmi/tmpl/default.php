<?php 
// No direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.formvalidator');
?>
	<form id="carica_programmi" class="form-validate <?php echo $class_sfx;?>" name="carica_programmi" method="post" enctype="multipart/form-data" />
	<!--anti-csrf
	https://docs.joomla.org/How_to_add_CSRF_anti-spoofing_to_forms-->
	<?php echo JHtml::_( 'form.token' ); ?>
		<table>

		<tbody width="100%">
			<tr>
				<td>Docente:</td>
				<td><input type="text" name="teacher" class="required" style="width: auto"/></td>
			</tr>
			<tr>
				<td>Materia:</td>
				<td><input type="text" name="subject" class="required" style="width: auto"/></td>
			</tr>
			<tr>
				<td>Classe:</td>
				<td>
					<select name="_class" style="width: auto">
					<?php
					$handle = $params->get('classes');
					echo $handle;
					$first_opt=true;
					foreach (preg_split("/((\r?\n)|(\r\n?))/", $handle) as $line){
						if ($line[0]=='#'){
							if (!$first_opt)
								echo "</optgroup>";
							//optgroup
						
							$group = substr($line, 1);
							
							echo "<optgroup label=$group>";
							$first_opt = false;
						}
						else
							echo "<option name=$line value=$line>$line</option>";
					}
					
					echo "</optgroup>";
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Programma:</td>
				<td><input type="file" id="doc" name="doc" value="doc" class="required" style="width: auto"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" style="width: auto"/></td>
			</tr>
		</tbody>
		</table>
	</form>
