<?php
defined('_JEXEC') or die('Restricted access');
//error_reporting (E_ALL);

class ModProgrammiHelper{
    static function getUser(){
    	$userF = JFactory::getUser();
        return $userF->name;
    }
    
    public static function validateString ($source){
		return (string)filter_var($source, FILTER_SANITIZE_STRING);
	}
	
	static function getCategoryOrCreate ($_dbo, $_table, $_parentId, $_title){
		$query = $_dbo->getQuery(true);
		
		$query
			->select($_dbo->quoteName(array('id', 'parent_id', 'title')))
			->from($_dbo->quoteName($_table))
			->where($_dbo->quoteName('title') . '='. $_dbo->quote($_title). " AND ".$_dbo->quoteName('parent_id') . '='. (int)$_parentId);
		
		$_dbo->setQuery($query);
		$result = $_dbo->loadAssoc();
		
		if (!$result){
			$query = $_dbo->getQuery(true);
			$columns = array('parent_id', 'title', 'date', 'published', 'access');
			$values  = array((int)$_parentId, $_dbo->quote($_title), $_dbo->quote('NOW()'), 1, 1);

			$query
				->insert($_dbo->quoteName($_table))
				->columns($_dbo->quoteName($columns))
				->values(implode(',', $values));
				
			
			$_dbo->setQuery($query);
			$_dbo->execute();
			
			$query = $_dbo->getQuery(true);
		
			$query
				->select($_dbo->quoteName(array('id', 'parent_id', 'title')))
				->from($_dbo->quoteName($_table))
				->where($_dbo->quoteName('title') . '='. $_dbo->quote($_title). " AND ".$_dbo->quoteName('parent_id') . '='. (int)$_parentId);
		
			$_dbo->setQuery($query);
			$result = $_dbo->loadAssoc();
		}
		return $result;
	}
    
	public static function processForm($post, $file, $params) {
		$year = $params->get('year');
	
		switch ((int)$params->get('folder')){
		case 0:
			$folder="piani_lavoro";
			$folder_phoca = "Piani di lavoro";
			break;
		case 1:
			$folder="programmi_finali";
			$folder_phoca = "Programmi finali";
			break;
		case 2:
			$folder="relazioni_finali";
			$folder_phoca = "Relazioni finali";
			break;
	
		}
		if ($file["error"] > 0)
			die("Error: " . $file["error"] . "<br>");

		$subject = self::validateString($post["subject"]);
		$class = self::validateString($post["_class"]);
		$teacher = self::validateString($post["teacher"]);
		
		$tmp_name=$file["tmp_name"];
		$mimetype=$file["type"];
		$size=$file["size"];
		
		$mbsize = $size/2000000;
		
		if ($mbsize>$size)
			die ("Filesize too big. Dimensione del file troppo grande");
		
		if ($mimetype!="application/pdf")
			die ("Only PDF document accepted. Sono accettati solo documenti in formato PDF");
				/*TODO
				SUPER_CHECK: per controllare che il mime type non sia fake, ed effettuare una validazione pro sull'input,
				è possibile verificare che i primi quattro caratteri equivalgano a %PDF
				*/
		
		$title = "$class, $subject, Prof $teacher";
		$dest_phoca = "documentazione_didattica/$folder/$year/$class/$title.pdf";
		
		//occhio ai path /web/htdocs/www.iisgalilei.eu/home/phocadownload
		$path_folder = "/web/htdocs/www.iisgalilei.eu/home/phocadownload/documentazione_didattica/$folder/$year/$class";
		$dest = "$path_folder/$title.pdf";
		
		if (!file_exists($path_folder)){
			if (!mkdir($path_folder, 0755, true))
				die('Failed to create folders... '.$path_folder);
		}
		
		if (!move_uploaded_file($tmp_name, $dest))
			die ("Internal Server Error 500.1".$tmp_name."path".$dest);
		
		if (!chmod ($dest, 0644))
			die ("Internal Server Error 500.2");
			
		$db = JFactory::getDbo();
		

		//get root category (didattica: id 11) TODO: backend selects prent category
		$result = self::getCategoryOrCreate($db, '#__phocadownload_categories', 11, $folder_phoca);
		$rootId = $result['id'];
	
		//get year category
		$result = self::getCategoryOrCreate($db, '#__phocadownload_categories', $rootId, $year);
		$yearId = $result['id'];
	
		//get class category
		$result = self::getCategoryOrCreate($db, '#__phocadownload_categories', $yearId, $class);
		$classId = $result['id'];
		
		//insert file entry
		$query = $db->getQuery(true);
		$columns = array('catid', 'title', 'filename', 'date', 'published', 'approved', 'access');
		$values  = array((int)$classId, $db->quote($title), $db->quote($dest_phoca), $db->quote('NOW()'), 1, 1, 1);
			$query
			->insert($db->quoteName('#__phocadownload'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));
		
		$db->setQuery($query);
		$db->execute();
		
		echo "<script>(function(){window.alert (\"Il file è stato caricato correttamente\")})();</script>";
	}
}
